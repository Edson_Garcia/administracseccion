package com.develop.administraSeccion.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.develop.administraSeccion.entity.Propietario;

@Repository
public interface IPropietarioRepository extends MongoRepository<Propietario, String>{

}
