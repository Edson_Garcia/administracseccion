package com.develop.administraSeccion.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.develop.administraSeccion.entity.Propietario;
import com.develop.administraSeccion.repository.IPropietarioRepository;


@Service
public class PropietarioServiceImpl implements PropietarioService{

	@Autowired
	private IPropietarioRepository propietarioRepository;

	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Propietario> findAll() {
		// TODO Auto-generated method stub
		return propietarioRepository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<Propietario> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return propietarioRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Propietario> findById(String curp) {
		return propietarioRepository.findById(curp);
	}

	@Override
	@Transactional
	public Propietario save(Propietario usuario) {
		return propietarioRepository.save(usuario);
	}

	@Override
	@Transactional
	public void deleteById(String curp) {
		propietarioRepository.deleteById(curp);		
	}
	
	
	
}
