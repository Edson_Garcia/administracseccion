package com.develop.administraSeccion.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.develop.administraSeccion.entity.Propietario;

public interface PropietarioService {

	public Iterable<Propietario> findAll();
	
	public Page<Propietario> findAll(Pageable pageable);
	
	public Optional<Propietario> findById(String curp);
	
	public Propietario save(Propietario usuario);
	
	public void deleteById(String curp);
}
