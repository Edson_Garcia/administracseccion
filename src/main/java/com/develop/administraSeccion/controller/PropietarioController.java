package com.develop.administraSeccion.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.develop.administraSeccion.entity.Propietario;
import com.develop.administraSeccion.service.PropietarioService;



@RestController
@RequestMapping("api/propietario")
public class PropietarioController {

	
	@Autowired
	private PropietarioService propietarioService;
	
	@PostMapping(value = "/registrar")
	public ResponseEntity<?> create(@RequestBody Propietario propietario){
		return ResponseEntity.status(HttpStatus.CREATED).body(propietarioService.save(propietario));
	}
	
	@GetMapping("/id")
	public ResponseEntity<?> read(@PathVariable(value="id") String usuarioId){
		Optional<Propietario> oUsuario = propietarioService.findById(usuarioId);
		if(!oUsuario.isPresent()){
			return ResponseEntity.notFound().build();
		}
			return ResponseEntity.ok(oUsuario);
	}
	
	@GetMapping("/getUsuarios")
	public ResponseEntity<Iterable<?>> readAll(){
		Iterable<Propietario> IUsuarios= propietarioService.findAll();
		return ResponseEntity.ok(IUsuarios);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/validarUsuario",consumes = "application/json",produces = "application/json")
	public @ResponseBody Propietario validarUsuario(@RequestBody Propietario user){
        Propietario prope = new Propietario();
        System.out.println("Entre al metodo");
        try {
        	System.out.println("Entre al try");
            if(user.getCurp().equals("gracie9456")){
                System.out.println("Entre al if");
                return prope;
            }else{
            	System.out.println("No encontre nada");
                return prope;
            }
        }catch (Exception e){
        	System.out.println(e);
        }
		return prope;
    }
}
