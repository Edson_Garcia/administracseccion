package com.develop.administraSeccion.principal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudApplication {

	public static void main(String[] args) throws Exception{
		SpringApplication.run(CrudApplication.class, args);
	}

}
