package com.develop.administraSeccion.entity;

import org.springframework.data.annotation.*;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "Propietarios")
public class Propietario {


    
    @Field(name = "nombre")
    private String nombre;
    @Field(name = "apellidos")
    private String apellidos;
    @Id
    @Field(name = "curp")
    private String curp;
    @Field(name = "edad")
    private int edad;
    
    
	public Propietario() {
		super();
	}
	
	public Propietario( String nombre, String apellidos, String curp, int edad) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.curp = curp;
		this.edad = edad;
	}

	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}

	@Override
	public String toString() {
		return "Propietario [ nombre=" + nombre + ", apellidos=" + apellidos + ", curp=" + curp
				+ ", edad=" + edad + "]";
	}
    
    
    
    
}
